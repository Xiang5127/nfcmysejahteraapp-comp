package com.example.nfcmysejahtera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    Button gotoRegisterButton_f, loginButton_f;
    EditText email_f, password_f;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onStart() {
        super.onStart();

        //if user login before, skip to main activity
        //but if they are from "logout", don't skip

        if(FirebaseAuth.getInstance().getCurrentUser() != null){
            Log.d("is_in", "not null");
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
        else{
            Log.d("is_out", "is null");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();

        email_f = findViewById(R.id.email_login);
        password_f = findViewById(R.id.password_login);

        loginButton_f = findViewById(R.id.loginBtn_login);
        loginButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //login user function
                loginUser();
            }
        });

        gotoRegisterButton_f = findViewById(R.id.registerBtn_login);
        gotoRegisterButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity_main.class));
            }
        });
    }

    private void loginUser(){
        String emailEntered_s = email_f.getText().toString();
        String password_s = password_f.getText().toString();

        if(emailEntered_s.isEmpty()){
            email_f.setError("Email Address is missing");
            Alert("Email Address not found");
            return;
        }
        if(password_s.isEmpty()){
            password_f.setError("Password is missing");
            Alert("Password not found");
            return;
        }

        //login user via firebase
        firebaseAuth.signInWithEmailAndPassword(emailEntered_s, password_s).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                //operation success;
                Alert("Welcome!");

                //if email verified, go to main
                //else verify email
                if(Objects.requireNonNull(firebaseAuth.getCurrentUser()).isEmailVerified()){
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
                else{
                    startActivity(new Intent(getApplicationContext(), VerifyEmailActivity.class).putExtra("email", emailEntered_s));
                }
                finish();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //operation failed
                Alert(e.getMessage());
            }
        });
    }

    private void Alert(String in){
        Toast.makeText(this, in, Toast.LENGTH_SHORT).show();
    }
}