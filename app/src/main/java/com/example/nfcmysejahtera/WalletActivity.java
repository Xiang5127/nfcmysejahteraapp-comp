package com.example.nfcmysejahtera;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class WalletActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseFirestore;
    String userUID;

    TextView userBalance;
    long originalAmount;
    StringBuilder s;

    List<String> originalArray;
    ArrayList<String> newArray;

    PendingIntent pendingIntent;
    IntentFilter[] writingTagFilters;
    NfcAdapter nfcAdapter;
    String operation;

    String studentID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        userUID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        userBalance = findViewById(R.id.userBalance_wall);

        showUserBalance();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this,0,new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writingTagFilters = new IntentFilter[] {tagDetected};
    }

    private void readFromIntent(Intent intent){
        String action = intent.getAction();
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)){
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if(rawMsgs != null){
                msgs = new NdefMessage[rawMsgs.length];
                for(int i=0; i<rawMsgs.length; i++){
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            buildTagViews(msgs);

            //do things here?????????????????????????????????????????????

            if(operation.charAt(0) == '+'){
                //add money
                Log.d("OppMoney", "Add");
                AddMoney();
            }
            else if(operation.charAt(0) == '-'){
                //pay money
                Log.d("OppMoney", "Pay");
                PayMoney();
            }
            else if(operation.charAt(0) == '#'){
                //eat #
                Log.d("OppMoney", "eat");

                EatMoney();
            }
            else{
                //not a money tag
                Alert("Invalid money tag");
            }
        }
    }

    private void EatMoney() {
        PayMoney();
        addItemToSheet();
    }

    private void AddMoney(){
        //update text view
        showUserBalance();

        //log original money
        Log.d("OppMoney", "original: " + originalAmount);

        //convert tag amount to int
        s = new StringBuilder();
        for(int i=1; i<operation.length(); i++){
            s.append(operation.charAt(i));
        }
        Log.d("OppMoney", "s: " + s);

        //add tag amount to original
        originalAmount += Integer.parseInt(s.toString());
        Log.d("OppMoney", "added: " + originalAmount);

        //update user balance in fire store
        DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
        documentReference.update(
                "Balance", originalAmount
        ).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //money added
                Alert("Money added: " + operation);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //failed
                Alert(e.getMessage());
            }
        });

        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                //get additional info
                studentID = Objects.requireNonNull(value.get("StudentID")).toString();
            }
        });

        //set history
        setHistoryMethod();

        Log.d("OppMoney", "Add done");
    }

    private void PayMoney(){
        //update text view
        showUserBalance();

        //log original money
        Log.d("OppMoney", "original: " + originalAmount);

        if(originalAmount == 0){
            //the user has no money left
            Alert("Current Balance is 0 !");
            return;
        }

        //convert tag amount to int
        s = new StringBuilder();
        for(int i=1; i<operation.length(); i++){
            s.append(operation.charAt(i));
        }
        Log.d("OppMoney", "s: " + s);

        //minus tag amount to original
        originalAmount -= Integer.parseInt(s.toString());
        Log.d("OppMoney", "pay: " + originalAmount);

        //update user balance in fire store
        DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
        documentReference.update(
                "Balance", originalAmount
        ).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //money added
                Alert("Money pay: " + operation);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //failed
                Alert(e.getMessage());
            }
        });

        //set history
        setHistoryMethod();

        Log.d("OppMoney", "Pay done");
    }

    private void setHistoryMethod(){
        //set history
        //update user history in fire store

        for(int i=0; i<originalArray.size(); i++){
            if(originalArray.get(i).equals("null")){
                originalArray.set(i, operation);
                break;
            }
        }

        DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
        documentReference.update(
                "Transaction", originalArray
        ).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //money added
                Alert("history set: " + operation);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //failed
                Alert(e.getMessage());
            }
        });

        Log.d("OppMoney", "257: " + originalArray.toString());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        readFromIntent(intent);
//        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
//            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writingTagFilters, null);
    }

    private void buildTagViews(NdefMessage[] msgs){
        //Set nfc content

        if(msgs == null || msgs.length == 0) return;

        String text = "";
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0)?"UTF-8":"UTF-16"; //Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; //Get the Language Code, e.g. "en"

        //Get the text
        text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1);

        this.operation = text;
        Log.d("OppMoney", "Scanned text: " + text);
    }

    private void showUserBalance(){
        //show user wallet balance
        DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                //set text
                userBalance.setText(String.format("Balance: %s", value.get("Balance")));
                originalAmount = (long) value.get("Balance");
                Log.d("OppMoney", "show success");

                originalArray = (List<String>) value.get("Transaction");
                assert originalArray != null;
                Log.d("OppMoney", "313: " + originalArray.toString());
            }
        });
    }

    private void addItemToSheet(){
        //I HAVE NO IDEA WHAT IS THIS

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://script.google.com/macros/s/AKfycbzZffSJJf3iWOemLvoMQm7x6_uriIxhlXMvQiQ4UATLTeju85dJTaCRcCnMWvmmfQzn/exec", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Alert("Success");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Alert(error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> paramsMap = new HashMap<>();

                paramsMap.put("action", "addData");
                paramsMap.put("studentNo", studentID);
                paramsMap.put("money", s.toString());

                return paramsMap;
            }
        };

        int socketTimeout =  50000; //5 sec
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }


    private void Alert(String in){
        Toast.makeText(this, in, Toast.LENGTH_SHORT).show();
    }


}