package com.example.nfcmysejahtera;

public class HistoryClass {

    private String operation;

    public HistoryClass(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
