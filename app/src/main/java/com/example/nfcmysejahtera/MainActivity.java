package com.example.nfcmysejahtera;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity{

    Button gotoProfile_f, testButton_f, gotoAttendance_f, gotoWallet_f, gotoHistory_f;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseFirestore;
    String name_f, ic_f, phone_f, email_f, userUID;

    public static final String Error_Detected = "No NFC Tag Detected";
    public static final String Write_Success = "Text Written Successfully!";
    public static final String Write_Error = "Error during Writing, Try Again";
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter[] writingTagFilters;
    boolean writeMode;
    Tag myTag;
    Context context;
    TextView edit_message, nfc_contents;
    Button ActivateButton;

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null){
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        userUID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        gotoProfile_f = findViewById(R.id.profileBtn_main);
        gotoProfile_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go to profile
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
        });

        testButton_f = findViewById(R.id.testSend_main);
        testButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncUserInfo();
                addItemToSheet();
            }
        });

        gotoAttendance_f = findViewById(R.id.attMode_main);
        gotoAttendance_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AttendanceActivity.class));
            }
        });

        gotoWallet_f = findViewById(R.id.balMode_main);
        gotoWallet_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), WalletActivity.class));
            }
        });

        gotoHistory_f = findViewById(R.id.history_main);
        gotoHistory_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HistoryActivity.class));
            }
        });

        edit_message = findViewById(R.id.writeTag_main);
        nfc_contents = findViewById(R.id.textfromTag_main);
        edit_message.setText("");
        nfc_contents.setText("");
        ActivateButton = findViewById(R.id.activateScan_main);
        context = this;
        ActivateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activate scan
                try {
                    if(myTag == null){
                        Alert(Error_Detected);
                    }else{
                        write(edit_message.getText().toString(), myTag);
                        Alert(Write_Success);
                    }
                }catch (IOException | FormatException e){
                    Alert(Write_Error);
                    e.printStackTrace();
                }
            }
        });
        //detect before button pressed ???????
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if(nfcAdapter == null){
            Alert("This device does not support NFC capabilities");
            //finish();
        }
        readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this,0,new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writingTagFilters = new IntentFilter[] {tagDetected};
    }

    private void readFromIntent(Intent intent){
        String action = intent.getAction();
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)){
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if(rawMsgs != null){
                msgs = new NdefMessage[rawMsgs.length];
                for(int i=0; i<rawMsgs.length; i++){
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            buildTagViews(msgs);

            //do things here?????????????????????????????????????????????
//            syncUserInfo();
//            addItemToSheet();
        }
    }

    private void buildTagViews(NdefMessage[] msgs){
        //read nfc content

        if(msgs == null || msgs.length == 0) return;

        String text;
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0)?"UTF-8":"UTF-16"; //Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; //Get the Language Code, e.g. "en"

        //Get the text
        try {
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
            Log.d("text", "in: " + text);
            nfc_contents.setText("NFC Content: " + text);
        } catch (UnsupportedEncodingException e) {
            Log.e("UnsupportedEncoding", e.toString());
        }
    }

    private void write(String text, Tag tag)throws IOException, FormatException{
        //real writing to the tag
        NdefRecord[] records = {createRecord(text)};
        NdefMessage message = new NdefMessage(records);
        //Get an instance of NDEF for the tag.
        Ndef ndef = Ndef.get(tag);
        //Enable I/O
        ndef.connect();
        //Write the message
        ndef.writeNdefMessage(message);
        Log.d("write", "Write to tag success: msg: " + message);
        //Close the connection
        ndef.close();
    }

    private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
        String lang = "en";
        byte[] textBytes = text.getBytes();
        byte[] langBytes = lang.getBytes("US_ASCII");
        int langLength = langBytes.length;
        int textLength = textBytes.length;
        byte[] payload = new byte[1 + langLength + textLength];

        //set status byte (see NDEF spec for actual bits)
        payload[0] = (byte) langLength;

        //copy langbytes and textbytes into payloads
        System.arraycopy(langBytes, 0, payload, 1, langLength);
        System.arraycopy(textBytes, 0, payload, 1+langLength, textLength);

        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        readFromIntent(intent);
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        WriteModeOff();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WriteModeOn();
    }

    //Enable Write
    private void WriteModeOn(){
        writeMode = true;
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writingTagFilters, null);
    }

    //Disable Wirte
    private void WriteModeOff(){
        writeMode = false;
        nfcAdapter.disableForegroundDispatch(this);
    }

    private void addItemToSheet(){
        //I HAVE NO IDEA WHAT IS THIS

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://script.google.com/macros/s/AKfycbyMJcHcbK-9x5IQwX8lL44uiZibcBVqfGLPssCf-ymT5l7FRPIFMtTETr8IspmVJVmqdQ/exec", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Alert("Success");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Alert("Failed?");
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> paramsMap = new HashMap<>();

                paramsMap.put("action", "addData");
                paramsMap.put("name", name_f);
                paramsMap.put("ic", ic_f);
                paramsMap.put("email", email_f);
                paramsMap.put("phone", phone_f);

                return paramsMap;
            }
        };

        int socketTimeout =  5000; //5 sec
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    private void syncUserInfo(){
        //get and store user info via fire store
        DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                //sync data
                if(value == null){
                    name_f = "null";
                    ic_f = "null";
                    email_f = "null";
                    phone_f = "null";
                }
                assert value != null;
                name_f = (String) value.get("FullName");
                ic_f = (String) value.get("ICNumber");
                email_f = (String) value.get("EmailAddress");
                phone_f = (String) value.get("PhoneNumber");
                Log.d("sync", name_f + " " + ic_f + " " + email_f + " " + phone_f);
            }
        });
    }

    private void Alert(String in){
        Toast.makeText(this, in, Toast.LENGTH_LONG).show();
    }
}
