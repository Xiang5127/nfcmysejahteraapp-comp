package com.example.nfcmysejahtera;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class recyclerAdapter extends RecyclerView.Adapter<recyclerAdapter.myViewHolder> {

    //set the actual list to be shown

    private ArrayList<HistoryClass> historyList;

    public recyclerAdapter(ArrayList<HistoryClass> history) {
        this.historyList = history;
    }

    public class myViewHolder extends RecyclerView.ViewHolder{
        private TextView operationTxt;

        public myViewHolder(final View view){
            super(view);

            operationTxt = view.findViewById(R.id.operation_history);
        }
    }

    @NonNull
    @Override
    public recyclerAdapter.myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_history, parent, false);
        return new myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull recyclerAdapter.myViewHolder holder, int position) {
        //real set here
        String operation = historyList.get(position).getOperation();
        Log.d("Re", "opp: " + operation);
        holder.operationTxt.setText(operation);
    }

    @Override
    public int getItemCount() {
        Log.d("Re", String.valueOf(historyList.size()));
        return historyList.size();
    }
}
