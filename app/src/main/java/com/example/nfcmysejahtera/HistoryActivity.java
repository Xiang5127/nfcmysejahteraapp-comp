package com.example.nfcmysejahtera;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HistoryActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseFirestore;
    String userUID;

    List<String> originalArray;

    private ArrayList<HistoryClass> historyList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        userUID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        recyclerView = findViewById(R.id.HistoryRecycler);
        historyList = new ArrayList<>();

        setOperationInfo();
        //setAdapter();
    }

    private void setAdapter() {
        //actual update
        Log.d("History", "in set adapter method");
        recyclerAdapter adapter = new recyclerAdapter(historyList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void setOperationInfo(){
        //getting from firebase and add item into list

        //show user wallet balance
        DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {

                if(value==null){
                    Log.d("History", "value null");
                }
                if(error==null){
                    Log.d("History", "error null");
                }

                HistoryActivity.this.originalArray = (List<String>) value.get("Transaction");
                assert originalArray != null;

                for(int i=0; i<originalArray.size(); i++){
                    if(originalArray.get(i).equals("null")){
                        break;
                    }
                    else{
                        historyList.add(new HistoryClass(originalArray.get(i)));
                    }
                }

                Log.d("History", "all original: " + originalArray.toString());
                Log.d("History", "all: " + historyList.toString());
                Log.d("History", "first content: " + historyList.get(0).getOperation());

                setAdapter();
                //Log.d("History", "fire-store error: " + error.getMessage());
            }
        });

//        for(int i=0; i<originalArray.size(); i++){
//            if(originalArray.get(i).equals("null")){
//                break;
//            }
//            else{
//                historyList.add(new HistoryClass(originalArray.get(i)));
//            }
//        }

    }
}