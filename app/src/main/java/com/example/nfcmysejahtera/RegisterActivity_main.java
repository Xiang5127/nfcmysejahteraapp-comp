package com.example.nfcmysejahtera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class RegisterActivity_main extends AppCompatActivity {

    EditText fullName_f, studentID_f, phoneNum_f, email_f, password_f, confPassword_f;
    String[] historyList;
    Button goButton_f, backButton_f;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseFirestore;
    String userUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_main);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        fullName_f = findViewById(R.id.fullName_reg);
        studentID_f = findViewById(R.id.studentID_reg);
        phoneNum_f = findViewById(R.id.mobileNum_reg);
        email_f = findViewById(R.id.email_reg);
        password_f = findViewById(R.id.password_reg);
        confPassword_f = findViewById(R.id.confpassword_reg);

        //go back to login
        backButton_f = findViewById(R.id.back_register);
        backButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        //verify and login
        goButton_f = findViewById(R.id.goBtn_reg);
        goButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }

    public void registerUser(){
        String nameEntered_s = fullName_f.getText().toString();
        String studentID = studentID_f.getText().toString();
        String phoneEntered_s = phoneNum_f.getText().toString();
        String emailEntered_s = email_f.getText().toString();
        String passwordEntered_s = password_f.getText().toString();
        String confPasswordEntered_s = confPassword_f.getText().toString();

        if(!checkInputValidation(nameEntered_s, studentID, phoneEntered_s, emailEntered_s, passwordEntered_s, confPasswordEntered_s)){
            return;
        }

        //done verifying all input from user
        Alert("Information registered");

        //register new user via firebase
        firebaseAuth.createUserWithEmailAndPassword(emailEntered_s, passwordEntered_s).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                //operation success

                //storing user data in fire store
                //Collection --> Document --> Field
                userUID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
                DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
                Map<String, Object> user = new HashMap<>();
                user.put("FullName", nameEntered_s);
                user.put("StudentID", studentID);
                user.put("PhoneNumber", "60" + phoneEntered_s);
                user.put("EmailAddress", emailEntered_s);
                user.put("Balance", 0);
                user.put("Transaction", Arrays.asList(historyList));
                documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("fire store", "Storing user success");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("fire store", "Storing user failed: " + e.getMessage());
                    }
                });

                startActivity(new Intent(getApplicationContext(), LoginActivity.class).putExtra("FromAddition", "NO"));
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //operation failed
                Alert(e.getMessage());
            }
        });
    }

    private boolean checkInputValidation(String nameEntered_s, String studentID_s, String phoneEntered_s, String emailEntered_s, String passwordEntered_s, String confPasswordEntered_s){
        //check name
        if(nameEntered_s.isEmpty()) {
            fullName_f.setError("Username not found");
            Alert("Please enter your username");
            return false;
        }
        else if(nameEntered_s.matches("[0-9]+")){
            fullName_f.setError("Username format invalid (did you included numbers? )");
            Alert("Username format invalid");
            return false;
        }

        //check student id
        if(studentID_s.isEmpty()){
            studentID_f.setError("Student ID not found");
            Alert("Please enter ID");
            return false;
        }
        else if(!studentID_s.matches("[0-9]+")){
            studentID_f.setError("Number format is invalid (did you add unnecessary alphabet ?)");
            Alert("ID format is invalid");
            return false;
        }
        else if(studentID_s.length() != 7){
            studentID_f.setError("ID length invalid (should be length of 7)");
            Alert("ID length invalid");
            return false;
        }

        //check phone
        if(phoneEntered_s.isEmpty()){
            phoneNum_f.setError("Mobile number not found");
            Alert("Mobile number not found");
            return false;
        }
        else if(!phoneEntered_s.matches("[0-9]+")){
            phoneNum_f.setError("Mobile number format invalid (make sure it only contains number and no spaces)");
            Alert("Mobile number format invalid");
            return false;
        }
        else if(phoneEntered_s.charAt(0)=='6' || phoneEntered_s.charAt(1)=='0'){
            phoneNum_f.setError("Mobile number format invalid (don't add \"60\")");
            Alert("Mobile number format invalid");
            return false;
        }
        else if(phoneEntered_s.charAt(0)=='0'){
            phoneNum_f.setError("Mobile number format invalid (should not start with \"0\")");
            Alert("Mobile number format invalid");
            return false;
        }

        //check email
        if(emailEntered_s.isEmpty()){
            email_f.setError("Email not found");
            Alert("Email not found");
            return false;
        }

        //check password
        if(passwordEntered_s.isEmpty()){
            password_f.setError("Password not found");
            Alert("Password not found");
            return false;
        }
        if(confPasswordEntered_s.isEmpty()){
            confPassword_f.setError("Confirm password not found");
            Alert("Confirm password not found");
            return false;
        }
        if(!confPasswordEntered_s.equals(passwordEntered_s)){
            Alert("Password is not equal to confirm password");
            return false;
        }

        //a history list of size 500 int value
        historyList = new String[500];
        //fill all with default value "null"
        Arrays.fill(historyList, "null");

        //everything is correct boi
        return true;
    }

    private void Alert(String in){
        Toast.makeText(this, in, Toast.LENGTH_SHORT).show();
    }
}