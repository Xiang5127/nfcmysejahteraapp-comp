package com.example.nfcmysejahtera;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AttendanceActivity extends AppCompatActivity{

    /***********************************************
     * THIS IS MAGIC !!!!!!!
     * IDK HOW THIS WORKS IT JUST DID WTH
     * AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
     * HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
     * HHHHHHHHHHHHHHHHHHH
     * ( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)
     * ───█───▄▀█▀▀█▀▄▄───▐█──────▄▀█▀▀█▀▄▄
     * ──█───▀─▐▌──▐▌─▀▀──▐█─────▀─▐▌──▐▌─█▀
     * ─▐▌──────▀▄▄▀──────▐█▄▄──────▀▄▄▀──▐▌
     * ─█────────────────────▀█────────────█
     * ▐█─────────────────────█▌───────────█
     * ▐█─────────────────────█▌───────────█
     * ─█───────────────█▄───▄█────────────█
     * ─▐▌───────────────▀███▀────────────▐▌
     * ──█──────────▀▄───────────▄▀───────█
     * ───█───────────▀▄▄▄▄▄▄▄▄▄▀────────█
     * ( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)
     */

    TextView test;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseFirestore;
    private static String name_f, id_f, phone_f , email_f , userUID;

    PendingIntent pendingIntent;
    IntentFilter[] writingTagFilters;
    NfcAdapter nfcAdapter;

    Button testBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attandance);

        test = findViewById(R.id.test_att);
        test.setText("attendance activity");

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        userUID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        testBtn = findViewById(R.id.testSend_att);
        testBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncUserInfo();
                addItemToSheet();
            }
        });

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this,0,new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writingTagFilters = new IntentFilter[] {tagDetected};
    }

    private void readFromIntent(Intent intent){
        String action = intent.getAction();
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)){
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if(rawMsgs != null){
                msgs = new NdefMessage[rawMsgs.length];
                for(int i=0; i<rawMsgs.length; i++){
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            buildTagViews(msgs);

            //do things here?????????????????????????????????????????????
            syncUserInfo();
            addItemToSheet();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        readFromIntent(intent);
//        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
//            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writingTagFilters, null);
    }

    private void buildTagViews(NdefMessage[] msgs){
        //Set nfc content

        if(msgs == null || msgs.length == 0) return;

        String text = "";
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0)?"UTF-8":"UTF-16"; //Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; //Get the Language Code, e.g. "en"

        //Get the text
        text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1);

        test.setText("NFC Content: " + text);
    }

    private void addItemToSheet(){
        //I HAVE NO IDEA WHAT IS THIS

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://script.google.com/macros/s/AKfycbxJrOzhNuVxXbMgXw6CTJIzoinxHdOlCwaAYlgVZIqO4hFvlO5qbRT0_diHXBKuz5XcTQ/exec", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Alert("Success");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Alert(error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.d("getparams", name_f);
                Map<String, String> paramsMap = new HashMap<>();

                paramsMap.put("action", "addData");
                paramsMap.put("name", name_f);
                paramsMap.put("id", id_f);
                paramsMap.put("email", email_f);
                paramsMap.put("phone", phone_f);

                return paramsMap;
            }
        };

        int socketTimeout =  50000; //5 sec
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    private void syncUserInfo(){
        //get and store user info via fire store
        DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                //sync data
//                if(value == null){
//                    name_f = "null";
//                    ic_f = "null";
//                    email_f = "null";
//                    phone_f = "null";
//                }
                name_f = value.get("FullName").toString();
                id_f = value.get("StudentID").toString();
                email_f = value.get("EmailAddress").toString();
                phone_f = value.get("PhoneNumber").toString();
                Log.d("sync", name_f + " " + id_f + " " + email_f + " " + phone_f);
            }
        });
    }

    private void Alert(String in){
        Toast.makeText(this, in, Toast.LENGTH_SHORT).show();
    }

}