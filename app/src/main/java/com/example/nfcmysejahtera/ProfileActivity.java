package com.example.nfcmysejahtera;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.Objects;

public class ProfileActivity extends AppCompatActivity {

    FirebaseFirestore firebaseFirestore;
    FirebaseAuth firebaseAuth;
    Button backToMainButton_f, logoutButton_f;
    public TextView name_f, id_f, email_f, phone_f;
    String userUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        backToMainButton_f = findViewById(R.id.backBtn_profile);
        backToMainButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //back to main activity
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        //TODO : weird crash after sign out

        logoutButton_f = findViewById(R.id.logoutBtn_profile);
        logoutButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //logout user
                firebaseAuth.signOut();
                //finish();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        name_f = findViewById(R.id.name_profile);
        id_f = findViewById(R.id.id_profile);
        email_f = findViewById(R.id.email_profile);
        phone_f = findViewById(R.id.phone_profile);
        userUID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        showUserDetails();
    }

    private void showUserDetails(){
        //show user details in text view
        DocumentReference documentReference = firebaseFirestore.collection("Users").document(userUID);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                //set text
                name_f.setText(String.format("Full Name: %s", value.get("FullName")));
                id_f.setText(String.format("Student ID: %s", value.get("StudentID")));
                email_f.setText(String.format("Email Address: %s", value.get("EmailAddress")));
                phone_f.setText(String.format("Mobile Number: %s", value.get("PhoneNumber")));
            }
        });
    }
}