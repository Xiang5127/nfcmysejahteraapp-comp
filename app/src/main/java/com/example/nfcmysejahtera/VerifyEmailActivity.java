package com.example.nfcmysejahtera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class VerifyEmailActivity extends AppCompatActivity {

    String emailExtra_s_f;
    TextView emailTxt_f;
    FirebaseAuth firebaseAuth;
    Button checkButton_f, resendButton_f, skipButton_f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);

        firebaseAuth = FirebaseAuth.getInstance();

        emailExtra_s_f = getIntent().getStringExtra("email");

        emailTxt_f = findViewById(R.id.email_verifyemail);
        emailTxt_f.setText(emailExtra_s_f);

        sendVerificationEmail();

        //TODO
        //PLEASE TODO !!!!!!!!!!!!!!!!!!!!!!!!!! TODO
        //TODO

        skipButton_f = findViewById(R.id.skipVerify_verifyEmail);
        skipButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //skip verifying email for now
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        checkButton_f = findViewById(R.id.emailvarifiedbtn_verifyemail);
        checkButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if email is verified
                if(firebaseAuth.getCurrentUser().isEmailVerified()){
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }else{
                    Alert("Email is not verified yet!");
                }
            }
        });

        resendButton_f = findViewById(R.id.resendemail_verifyemail);
        resendButton_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //resend verification email
                sendVerificationEmail();
            }
        });
    }

    private void sendVerificationEmail(){
        Objects.requireNonNull(firebaseAuth.getCurrentUser()).sendEmailVerification().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //success
                Alert("Verification email send");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //failed
                Alert(e.getMessage());
            }
        });
    }

    private void Alert(String in){
        Toast.makeText(this, in, Toast.LENGTH_SHORT).show();
    }
}